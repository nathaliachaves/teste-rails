require 'rails_helper'

RSpec.describe AvailableProduct, type: :model do
  subject { described_class.new }

  describe 'associations' do
    it { should belong_to(:product) }
    it { should belong_to(:color) }
    it { should belong_to(:size) }
  end 

  describe 'validations' do
    it { should validate_presence_of(:color) }
    it { should validate_presence_of(:size) }
    it { should validate_presence_of(:quantity) }
  end
end   