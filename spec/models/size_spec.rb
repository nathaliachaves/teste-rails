require 'rails_helper'

RSpec.describe Size, type: :model do
  subject { described_class.new }

  describe 'associations' do
    it { should have_many(:available_products) }
  end 

  describe 'validations' do
    it { should validate_presence_of(:name) }
  end
end   