FactoryBot.define do
  factory :available_product do
    association :product
    association :color
    association :size
    
    quantity { 10 }
  end
end