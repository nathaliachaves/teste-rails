FROM ruby:2.3.8

RUN apt-get update && \
    apt-get -y dist-upgrade && \
    apt-get install -y libpq-dev && \
    curl -sL https://deb.nodesource.com/setup_8.x | bash && \
    apt-get install -y nodejs && \
    apt-get install -y postgresql-client && \
    apt-get -y clean && \
    find /var/lib/apt/lists/ -type f -exec rm -f {} \;

RUN mkdir /teste-rails
WORKDIR /teste-rails

COPY Gemfile /teste-rails/Gemfile
COPY Gemfile.lock /teste-rails/Gemfile.lock

RUN bundle install

COPY . /teste-rails