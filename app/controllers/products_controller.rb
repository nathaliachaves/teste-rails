class ProductsController < ApplicationController

  def index
    @products = Product.all
  end

  def new
    @product = Product.new
    @product.build_product_relations
  end

  def create
    @product = Product.new(product_params)

    if @product.save
      redirect_to products_path, notice: 'Produto Cadastrado.'
    else
      flash.now[:error] = "Could not save client"
      render :new
    end
  end

  def edit
    @product = Product.find(params[:id]) 
  end

  def update
    @product = Product.find(params[:id])

    if @product.update(product_params)
      if params[:remove_available_products].present?
        params[:remove_available_products].each do |id|
          @product.available_products.destroy(AvailableProduct.find id.to_i)
        end
      end

      redirect_to products_path, notice: 'Produto Atualizado.'
    else
      render :new
    end
  end

  def destroy
    Product.find(params[:id]).destroy

    redirect_to products_path, notice: 'Produto Removido.'
  end

  private

  def product_params
    params.require(:product).permit(:name, :description, available_products_attributes: [:id, :color_id, :size_id, :quantity, :_destroy])
  end

end