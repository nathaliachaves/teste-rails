class Size < ActiveRecord::Base
  has_many :available_products

  validates :name, presence: true
end
