class AvailableProduct < ActiveRecord::Base
  belongs_to :product
  belongs_to :color
  belongs_to :size
  
  validates :color, :size, :quantity,  presence: true  
end
