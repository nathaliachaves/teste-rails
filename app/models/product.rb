class Product < ActiveRecord::Base
  has_many :available_products, dependent: :destroy 
  accepts_nested_attributes_for :available_products, reject_if: :all_blank, allow_destroy: true

  validates :name, presence: true

  def build_product_relations
    available_products.build if available_products.blank?
  end   
end
