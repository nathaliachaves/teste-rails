products_colors = [
  'Amarelo',
  'Ameixa',
  'Ametista',
  'Anil',
  'Azul',
  'Mostarda',
  'Negro',
  'Ocre',
  'Oliva',
  'Ouro',
  'Prata',
  'Preto',
  'Rosa',
  'Roxo',
  'Rubro',
  'Terracota',
  'Tijolo',
  'Turquesa',
  'Uva',
  'Verde',
  'Vermelho',
  'Vinho',
  'Violeta'
]

products_colors.each do |color|
  Color.create!(name: color)
end   

products_sizes = [
  'PP',
  'P',
  'M',
  'G',
  'GG',
]

products_sizes.each do |size|
  Size.create!(name: size)
end   